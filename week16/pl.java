import java.io.*;
import java.util.*;



public class pl{
    

    public static void main(String[] args) throws FileNotFoundException{
    
        String infile = args[0];
        Scanner s = new Scanner(new File(infile)); 
        String[] array = new String[1000000];
                int k = 0;
                while (s.hasNext()){
                    array[k] = s.next(); 
                    k++;
            }
        s.close();
            //** Heap data structure */

        PriorityQueue<String> names = new PriorityQueue<>();
        for (String item : array) {
            names.add(item);
        }
        //**Heap sort */

        ArrayList<String> list=new ArrayList<String>();

        long tStart = System.currentTimeMillis();
        Iterator itr = names.iterator();
        while(itr.hasNext()){
            list.add(names.poll());                

        }
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        System.out.print("Heap took: ");
        System.out.println(tDelta + " msec");

        /**Queue Data Structure */

        String infile2 = args[1];
        Scanner u = new Scanner(new File(infile2));
        String[] array2 = new String[1000000];
        ArrayList<String> list2=new ArrayList<String>();
                int y = 0;
                while (u.hasNext()){
                    array2[y] = u.next(); 
                    y++;
            }
        u.close();

        /**Queue Sort */

        long tStart2 = System.currentTimeMillis();
        Arrays.sort(array2);
      
        Queue<String> names2 = new LinkedList<String>();
        for (String item : array2) {

            names2.add(item);

        }
        long tEnd2 = System.currentTimeMillis();
        long tDelta2= tEnd2- tStart2;
        System.out.print("Queue took: ");
        System.out.println(tDelta2+" msec");
    
    
    
    }}
